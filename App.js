import React, { Component } from 'react'

import PostsList from './Components/PostsList'

class App extends Component{
    render() {
        return (
            <div className ="App">
               <PostsList/>
            </div>
        )
    }
}
export default App