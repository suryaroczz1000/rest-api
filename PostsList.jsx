import React, { useEffect,useState } from 'react'
import axios from 'axios'

const PostsList = () => {

    const [posts, setPosts] = useState([]);
    const [error, setError] = useState('');

    useEffect(()=>{
        axios('https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/')
    .then(res=>setPosts(res.data.results))
    .catch(err=>setError(err));
    },[]);
 console.log(posts);
   

    return (
        <div>
           List of posts
           {
               posts.length ?
                posts.map((post,index) => <div key={index}>{post.name}</div>) :
               null
           }
           { error ? <div>error found</div> : null}
        </div>
    );
}

export default PostsList